
import pygame

from colors import *

class Background(pygame.sprite.Sprite):
	def __init__(self, camera, img_filename, x, y):
		# Call the parent's constructor
#		super().__init__()
		pygame.sprite.Sprite.__init__(self)

		self.image=pygame.image.load(img_filename).convert()
		self.image.set_colorkey( COLORS.transparent )

		# Make our top-left corner the passed-in location.
		self.rect = self.image.get_rect()

		self.position = pygame.Rect(x, y, 0, 0)

		self.camera=camera

	def update(self):
		self.rect=self.camera.apply(self.position)

