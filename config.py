#!/usr/bin/env python

import argparse
import os

class Config():
	sound_volume = 100.0
	sound_fadein = 1000
	frame_rate = 30.0

	# Screen dimensions
	screen_width = 1024
	screen_height = 560

	# Ativar debug se > 0
	DEBUG=0


	def load_cli(self):
		parser = argparse.ArgumentParser()

		screen_size=str(self.screen_width)+"x"+str(self.screen_height)

		parser.add_argument("--volume", type=float, default=self.sound_volume,
			metavar=self.sound_volume, help="Set volume level")
		parser.add_argument("--debug","-d", nargs='?', const=1, default=self.DEBUG,
			metavar='LEVEL', help="Enable debug level mode")
		parser.add_argument("--screen","-s", type=str, default=screen_size,
			metavar=screen_size, help="Set screen size WxH")

		args = parser.parse_args()

		# Set volume
		self.sound_volume = args.volume

		# Enable debug
		self.DEBUG = args.debug

		# Set screen size
		screen_size=args.screen
		self.screen_width=int(screen_size.split('x')[0])
		self.screen_height=int(screen_size.split('x')[1])


	def load_sys_env(self):
		# Set volume
		self.sound_volume=float(os.getenv('SOUND_VOLUME', self.sound_volume))

		# Enable debug
		self.DEBUG=int(os.getenv('DEBUG', self.DEBUG))
		
		# Set screen size
		screen_size=str(self.screen_width)+"x"+str(self.screen_height)
		screen_size=os.getenv('SCREEN_SIZE', screen_size)
		self.screen_width=int(screen_size.split('x')[0])
		self.screen_height=int(screen_size.split('x')[1])


	def __init__(self):
		self.load_cli()
		self.load_sys_env()

