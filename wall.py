
import pygame
import pymunk

from colors import *

class Wall(pygame.sprite.Sprite):
	""" Wall the player can run into. """
	def __init__(self, camera, f, x, y, width, height, color=COLORS.transparent):
		""" Constructor for the wall that the player can run into. """
		# Call the parent's constructor
#		super().__init__()
		pygame.sprite.Sprite.__init__(self)
		# Make a wall, of the size specified in the parameters
		self.image = pygame.Surface([width, height])
		self.image.fill( color )

		# Make our top-left corner the passed-in location.
		self.rect = self.image.get_rect()
		self.rect.y = y
		self.rect.x = x

		self.body = f.create_body(1,1000,pymunk.Body.STATIC)
#		self.body = f.space.static_body
		self.body.position = x,y

		self.shape = f.create_segment(x, y, width, height, self.body)
		f.add(self.shape)

		self.camera=camera

	def update(self):
		self.rect.y=self.shape.body.position.y
		self.rect.x=self.shape.body.position.x

		self.rect=self.camera.apply(self.rect)

