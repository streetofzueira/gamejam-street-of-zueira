#!/usr/bin/env python

import pymunk
import math

class Physics():
	def __init__(self, xgravity=0.0, ygravity=0.0):
		self.step_size=50
		self.space = pymunk.Space()
		self.space.gravity = (xgravity, ygravity)

	def step(self):
		self.space.step(1/self.step_size)

	def add(self,*objs):
		self.space.add(objs)

	def create_body(self,mass=1, moment=1000, btype=pymunk.Body.DYNAMIC):
		ret=pymunk.Body(mass, moment, btype)
		ret.elasticity=0.0001
		return ret

	def create_rectangle(self, body, rect):
		ret=pymunk.Poly.create_box(body, rect.size)
		ret.elasticity=0.0001
		return ret

	def create_poly(self, body, vertices):
		ret=pymunk.Poly(body, vertices)
		ret.friction=20
		return ret
		

	def create_segment(self, x, y, width, height, body):
		ret=pymunk.Segment(body, (x, y), (x+width, y+height), 5)
#		ret.elasticity=0.0001
		return ret

	def create_line(self, begin, end, body, size=20):
		ret=pymunk.Segment(body, begin, end, size/2)
		return ret

