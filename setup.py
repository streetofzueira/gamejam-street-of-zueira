from setuptools import setup, find_packages
setup(
	name = "Rage of the Zueira",
	version = "0.1",
	packages = find_packages(),
	scripts = ['__init__.py'],

	# Project uses reStructuredText, so ensure that the docutils get
	# installed or upgraded on the target machine
	install_requires = ['docutils>=0.3'],

	package_data = {
		'': ['media/img/*.png', 'media/img/*.jpg', 'media/sound/*.ogg'],
		# And include any *.msg files found in the 'hello' package, too:
		# 'hello': ['*.msg'],
	},

    # metadata for upload to PyPI
    author = "Rage of the Zueira Team",
	author_email = "psycho.mantys@gmail.com",
	description = "Jogo 2d em python feito no game Jam",
	license = "GPL2",
	keywords = "arcade 2d Beat'em_up",
	url = "https://github.com/psychomantys/gamejam-Street-of-Zueira/",   # project home page, if any

	# could also include long_description, download_url, classifiers, etc.
)
