#!/usr/bin/env python

import pygame
import pymunk

from colors import *
from utils import *

class Line(pygame.sprite.Sprite):
	image=None
	config=None
	size=None

	""" Wall the player can run into. """
	def __init__(self, config, camera, f, begin, end, color=COLORS.transparent, size=10):
		# Call the parent's constructor
		pygame.sprite.Sprite.__init__(self)

		self.config=config
		self.size=size

		if begin[0]>end[0]:
			aux=end
			end=begin
			begin=aux

		self.image=pygame.Surface( (abs(end[0]-begin[0])+size, abs(end[1]-begin[1])+size) )

		self.image.fill(COLORS.transparent)
		self.image.set_colorkey( COLORS.transparent )

		# Debug
		debug_image_rect(config, self)

		self.rect = self.image.get_rect()
		self.rect.x = begin[0]
		self.rect.y = begin[1]

		self.body = f.create_body(1,1000,pymunk.Body.STATIC)

		if begin[1]<end[1]:
			pygame.draw.line(self.image, color, (0+(size/2),0), ((size/2)+end[0]-begin[0],end[1]-begin[1]), size)
			self.shape = f.create_line( (begin[0]+(size/2),begin[1]), (end[0]+(size/2),end[1]), self.body, size)
		else:
			p1=(0,0+abs(end[1]-begin[1])+(size/2) )
			p2=(end[0]-begin[0],end[1]-begin[1]+abs(end[1]-begin[1])+(size/2))

			pygame.draw.line(self.image, color, p1, p2, size)
			begin=(begin[0]-size/2,begin[1]-size/2)
			self.shape=f.create_line( (begin[0]+(size/2),begin[1]), (end[0],end[1]-size/2), self.body, size)

		f.add(self.shape)

		self.camera=camera

	def update(self):
		self.update_physics()
		self.update_cam()


	def update_physics(self):
		### Dando update na imagem com a fisica....
		pv1 = self.body.position + self.shape.a.rotated(self.body.angle)
		pv2 = self.body.position + self.shape.b.rotated(self.body.angle)

		if pv1[1]<pv2[1]:
			self.rect.x=pv1[0]-(self.size/2)
			self.rect.y=pv1[1]
		else:
			self.rect.x=pv1[0]
			self.rect.y=pv1[1]-(self.size/2)-abs(pv1[1]-pv2[1])

		# Tentando fazer com que ele pare de girar 
		self.body.angle=0

	def update_cam(self):
		self.rect=self.camera.apply(self.rect)

