#!/usr/bin/env python


import pygame

from colors import *
from utils import *

class Player(pygame.sprite.Sprite):
	""" This class represents the bar at the bottom that the player
	controls. """

	config=None

	def __init__(self, config, camera, f, x, y):
		# Call the parent's constructor
		pygame.sprite.Sprite.__init__(self)

		self.config=config

		self.images = {'stop':[], 'walk':[]}
		

		self.images['stop'].append(load_image(rfn("media/img/player_stop.png"),-1))


		for aux in range(1,12):
			img_filename=rfn("media/img/player_walk_"+str(aux).rjust(2,'0')+".png")
			self.images['walk'].append(load_image(img_filename,-1))

		self.image = self.images['stop'][0]
		self.index = 0
		self.animation_mode='stop'

		# Make our top-left corner the passed-in location.
		self.rect = self.image.get_rect()
		self.rect.center = x,y

		self.is_go_right=True

		self.body = f.create_body()
		self.body.position = x,y

		#self.shape = f.create_rectangle(self.body, self.rect)
		self.shape = f.create_poly(self.body, [(-25,25), (-25,53), (25,53), (25,25)])
		f.add(self.body, self.shape)

		self.camera=camera

		
		debug_image_rect(config, self)


	# Muda orientacao para a direita
	def go_right(self):
		if not self.is_go_right:
			self.image=pygame.transform.flip(self.image,True,False)
			self.is_go_right=True

	# Muda orientacao para a esquerda
	def go_left(self):
		if self.is_go_right:
			self.image=pygame.transform.flip(self.image,True,False)
			self.is_go_right=False

	# Muda a animacao para alguma outra
	def change_animation(self, mode):
		self.animation_mode=mode

	def change_speed(self, x, y):
		""" Change the speed of the player. """
		self.body.velocity.x=x
		self.body.velocity.y=y

	def change_yspeed(self, y):
		""" Change the speed of the player on y axis. """
#		self.body.velocity.y=y
		self.body.velocity=(self.body.velocity.x, y)

	def change_xspeed(self, x):
		""" Change the speed of the player on x axis. """
#		self.body.velocity.x=x
		self.body.velocity=(x, self.body.velocity.y)


	def update(self):

		self.update_physics()
		self.update_cam()

		######## Anima a imagem
		self.index += 1
		if self.index >= len(self.images[self.animation_mode])*10:
			self.index = 0

		if( self.is_go_right ):
			self.image = self.images[self.animation_mode][int(self.index/10)]
		else:
			self.image = pygame.transform.flip(self.images[self.animation_mode][int(self.index/10)], True, False)

	def update_physics(self):
		### Dando update na imagem com a fisica....
		self.rect.center= self.body.position.x, self.body.position.y
		# Tentando fazer com que ele pare de girar 
		self.body.angle=0

	def update_cam(self):
		self.rect=self.camera.apply(self.rect)

