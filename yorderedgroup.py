
import pygame

def get_key_y(a):
	return a.rect.bottom

class YRenderUpdates(pygame.sprite.Group):
	def draw(self, surface):
		"""draw all sprites onto the surface

		Group.draw(surface): return None

		Draws all of the member sprites onto the given surface.

		"""
		sprites = self.sprites()
		sprites.sort(key=get_key_y)
		surface_blit = surface.blit
		for spr in sprites:
			self.spritedict[spr] = surface_blit(spr.image, spr.rect)
		self.lostsprites = []

