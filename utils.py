#!/usr/bin/env python

import pygame
import pymunk
from colors import *

def rfn(filename):
	from pkg_resources import resource_filename
	return resource_filename(__name__, filename)


def static_vars(**kwargs):
	""" Define variaveis estaticas """
	def decorate(func):
		for k in kwargs:
			setattr(func, k, kwargs[k])
		return func
	return decorate

class Point:
	def __init__(self, x=0, y=0):
		self.x=x
		self.y=y

"""
Le uma imagem

	\param filename Nome do arquivo a se lido
	\param colorkey Cor a ser transparente. Se -1, usa o primeiro pixel da imagem.
	\return imagem que foi carregada

"""
def load_image(filename, colorkey=None):
	try:
		image = pygame.image.load(filename)
	except(pygame.error, message):
		print('Cannot load image:'+filename)
		raise(SystemExit, message)
	image = image.convert()
	if colorkey is not None:
		if colorkey is -1:
			colorkey = image.get_at((0,0))
		image.set_colorkey(colorkey)
	return image


def debug_image_rect(config,sprite):
	if config.DEBUG:
		pygame.draw.rect(sprite.image, COLORS.pink, sprite.image.get_rect(), 3)


@static_vars(grid=None)
def debug_grid(config, screen=pygame.display.get_surface()):
	if config.DEBUG:
		if not debug_grid.grid:
			# Cria imagem e atribui transparencia
			grid=pygame.Surface( (config.screen_width, config.screen_height) )
			grid.fill(COLORS.transparent)
			grid.set_colorkey( COLORS.transparent )

			font = pygame.font.SysFont( "fonte.ttf" , 20 )
			font.set_underline( True )

			for x in range(0, config.screen_width, 100):
				pygame.draw.line(grid, COLORS.pink, (x,0), (x,config.screen_height), 1)
				grid.blit(font.render(str(x),True,COLORS.yellow), (x, 0))
				grid.blit(font.render(str(x),True,COLORS.yellow), (x, config.screen_height-30))

			for y in range(0, config.screen_height, 100):
				pygame.draw.line(grid, COLORS.pink, (0,y), (config.screen_width,y), 1)
				grid.blit(font.render(str(y),True,COLORS.yellow), (0, y))
				grid.blit(font.render(str(y),True,COLORS.yellow), (config.screen_width-30, y))
			
			debug_grid.grid=grid.convert()

		screen.blit(debug_grid.grid, (0,0))

@static_vars(draw_options=None)
def debug_draw_physics(config, physics, screen=pygame.display.get_surface()):
	if config.DEBUG:
		if not debug_draw_physics.draw_options :
			pymunk.pygame_util.positive_y_is_up=False
			debug_draw_physics.draw_options=pymunk.pygame_util.DrawOptions(screen)
		physics.space.debug_draw(debug_draw_physics.draw_options)

@static_vars(template="{} - FPS: {:.2f}")
def debug_show_fps(config, clock):
	if config.DEBUG:
		caption=debug_show_fps.template.format("Street of the Zueira", clock.get_fps())
		pygame.display.set_caption(caption)


