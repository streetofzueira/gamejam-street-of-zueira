#!/usr/bin/env python

import sys

import pygame
import pymunk
import pymunk.pygame_util

from player import *
from wall import *
from line import *
from background import *

from utils import *
from config import *

from colors import *
from physics import *
from camera import *

from yorderedgroup import *

def main():

	# Load config from comand line, vars and config file
	config=Config()

	# Call this function so the Pygame library can initialize itself
	pygame.init()
	physics=Physics()
	camera = Camera(complex_camera, config.screen_width, config.screen_height, config.screen_width*3, config.screen_height*2)

	# Create an sized screen
	screen = pygame.display.set_mode([config.screen_width, config.screen_height])
	# Set the title of the window
	pygame.display.set_caption('Street of the Zueira')

	# List to hold all the sprites
	background_layer = pygame.sprite.RenderUpdates()
	#middle_layer = pygame.sprite.RenderUpdates()
	middle_layer = YRenderUpdates()
	front_layer = pygame.sprite.RenderUpdates()

	# Load the background image. Street asset was taken from Opengameart.org.
	background = Background(camera, rfn('media/img/street.png'), 0, 290)
	background_layer.add(background)
	background2 = Background(camera, rfn('media/img/street.png'), 450, 290)
	background_layer.add(background2)

	myself = Background(camera, rfn('media/img/player_attack_01.png'), 450, 200)
	middle_layer.add(myself)

	myself2 = Player(config, camera, physics, 500, 200)
	middle_layer.add(myself2)

	# Make the walls.
	wall = Line(config, camera, physics, (400,400) , (100,100) , COLORS.red, 25)
	front_layer.add(wall)
	
	wall = Line(config, camera, physics, (50,50) , (50,150) , COLORS.red, 25)
	front_layer.add(wall)

	wall = Line(config, camera, physics, (50,150) , (50,350) , COLORS.red, 25)
	front_layer.add(wall)

	wall = Line(config, camera, physics, (0,100) , (100,100) , COLORS.red, 5)
	front_layer.add(wall)

	wall = Line(config, camera, physics, (500,200) , (800,100) , COLORS.red, 25)
	front_layer.add(wall)

	wall = Line(config, camera, physics, (600,400) , (800,400) , COLORS.green, 25)
	middle_layer.add(wall)

#	wall = Line(config, camera, physics, (900,100) , (800,200) , COLORS.green, 35)
#	front_layer.add(wall)
	wall = Line(config, camera, physics, (800,200) , (900,100) , COLORS.green, 35)
	front_layer.add(wall)

	# Create the player paddle object
	player = Player(config, camera, physics, 400.0, 200.0)
	middle_layer.add(player)

	clock = pygame.time.Clock()
	done = False

	# Load and play the soundtrack.
	soundtrack = pygame.mixer.Sound(rfn("media/sound/song_01.ogg"))
	soundtrack.set_volume(config.sound_volume)
	soundtrack.play(-1, 0, config.sound_fadein)

	keep_xmoving=0
	keep_ymoving=0
	
	while not done:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				done = True
	
			elif event.type == pygame.KEYDOWN:
				if event.key == pygame.K_LEFT or event.key == pygame.K_a:
					player.change_xspeed(-150.0)
					player.go_left()
					player.change_animation('walk')
					keep_xmoving+=1
				elif event.key == pygame.K_RIGHT or event.key == pygame.K_d:
					player.change_xspeed(150.0)
					player.go_right()
					player.change_animation('walk')
					keep_xmoving+=1
				elif event.key == pygame.K_UP or event.key == pygame.K_w:
					player.change_yspeed(-150.0)
					player.change_animation('walk')
					keep_ymoving+=1
				elif event.key == pygame.K_DOWN or event.key == pygame.K_s:
					player.change_yspeed(150.0)
					player.change_animation('walk')
					keep_ymoving+=1
				elif event.key == pygame.K_ESCAPE:
					done = True

			elif event.type == pygame.KEYUP:
				if event.key == pygame.K_LEFT or event.key == pygame.K_a:
					keep_xmoving-=1
					if not keep_xmoving:
						if not keep_ymoving:
							player.change_animation('stop')
						player.change_xspeed(0)
						player.go_left()
					else:
						player.go_right()
						player.change_xspeed(150)
				elif event.key == pygame.K_RIGHT or event.key == pygame.K_d:
					keep_xmoving-=1
					if not keep_xmoving:
						if not keep_ymoving:
							player.change_animation('stop')
						player.change_xspeed(0)
						player.go_right()
					else:
						player.go_left()
						player.change_xspeed(-150)
				elif event.key == pygame.K_UP or event.key == pygame.K_w:
					keep_ymoving-=1
					if not keep_ymoving:
						if not keep_xmoving:
							player.change_animation('stop')
						player.change_yspeed(0)
					else:
						player.change_yspeed(150)
				elif event.key == pygame.K_DOWN or event.key == pygame.K_s:
					keep_ymoving-=1
					if not keep_ymoving:
						if not keep_xmoving:
							player.change_animation('stop')
						player.change_yspeed(0)
					else:
						player.change_yspeed(-150)
						



		camera.update(player.body.position)

		background_layer.update()
		middle_layer.update()
		front_layer.update()
		
		screen.fill(COLORS.black)

		background_layer.draw(screen)
		middle_layer.draw(screen)
		front_layer.draw(screen)


		# Debug
		debug_show_fps(config, clock)
		debug_draw_physics(config, physics, screen)
		debug_grid(config, screen)

		pygame.display.flip()
		clock.tick(config.frame_rate)
		physics.step()

	pygame.quit()

if __name__ == "__main__":
	# execute only if run as a script
	sys.exit(main())

